function areaOfTriangles(base, height) {
    return (base * height) / 2
}

console.log(areaOfTriangles(3, 2));
console.log(areaOfTriangles(7, 4));
console.log(areaOfTriangles(10, 10));